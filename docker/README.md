# How to build

Build docker image
```shell
docker build -t weather2influx:test -f docker/Dockerfile .
```

Create an env file to test your built image,
like the development one: [.env.development](../.env.development)

# Test built image
```shell
docker run --rm --env-file <env-file> weather2influx:test
```
