import { Point, type WriteApi } from '@influxdata/influxdb-client'

import { ENV, netatmoConfig } from './config.js'
import { Netatmo } from './netatmo.js'
import { type Module } from './netatmo/interfaces.js'
import { createPollingJob } from './polling.js'

const humidex = (temperature: number, humidity: number) => {
  const t = (7.5 * temperature) / (237.7 + temperature)
  const et = Math.pow(10, t)
  const e = 6.112 * et * (humidity / 100)
  const humidex = temperature + (5 / 9) * (e - 10)

  return humidex < temperature ? temperature : humidex
}

const modulesArrayToMap = (modules: Module[]): Map<string, Module> => {
  const modulesMap = new Map<string, Module>()
  modules
    .filter((module) => module.dashboard_data !== undefined)
    .forEach((module) => {
      if (module.module_name === undefined) modulesMap.set('external', module)
      else modulesMap.set(module.module_name, module)
    })
  return modulesMap
}

const workerFn = (influxWrite: WriteApi): (() => Promise<void>) => {
  const netatmo = new Netatmo(netatmoConfig)

  return async () => {
    const stationsData = await netatmo.getStationsData()
    if (stationsData.body.body.devices.length === 0) {
      console.error('Netatmo returned no devices', stationsData.body.body)
      return
    }
    const points = []
    const firstDevice = stationsData.body.body.devices[0]

    points.push(
      new Point('internal')
        .timestamp(new Date(firstDevice.dashboard_data.time_utc * 1000))
        .floatField('temperature', firstDevice.dashboard_data.Temperature)
        .intField('humidity', firstDevice.dashboard_data.Humidity)
        .intField('noise', firstDevice.dashboard_data.Noise)
        .floatField(
          'humidex',
          humidex(
            firstDevice.dashboard_data.Temperature,
            firstDevice.dashboard_data.Humidity,
          ),
        ),
    )

    const modules = modulesArrayToMap(firstDevice.modules)
    const externalModule = modules.get('external')
    const bedroomModule = modules.get('bedroom')

    if (externalModule?.dashboard_data !== undefined) {
      points.push(
        new Point('external')
          .timestamp(new Date(externalModule.dashboard_data.time_utc * 1000))
          .floatField('temperature', externalModule.dashboard_data.Temperature)
          .intField('humidity', externalModule.dashboard_data.Humidity)
          .intField('battery', externalModule.battery_vp)
          .floatField(
            'humidex',
            humidex(
              externalModule.dashboard_data.Temperature,
              externalModule.dashboard_data.Humidity,
            ),
          ),
      )
    } else {
      console.error('Netatmo returned no external sensor')
    }

    if (bedroomModule?.dashboard_data !== undefined) {
      points.push(
        new Point('bedroom')
          .timestamp(new Date(bedroomModule.dashboard_data.time_utc * 1000))
          .floatField('temperature', bedroomModule.dashboard_data.Temperature)
          .intField('humidity', bedroomModule.dashboard_data.Humidity)
          .intField('battery', bedroomModule.battery_vp)
          .floatField(
            'humidex',
            humidex(
              bedroomModule.dashboard_data.Temperature,
              bedroomModule.dashboard_data.Humidity,
            ),
          ),
      )
    } else {
      console.error('Netatmo returned no bedroom sensor')
    }

    influxWrite.writePoints(points)
  }
}

const REPEAT_EVERY = 200 * 1000 // 3:20 min:ss

const netatmo = createPollingJob({
  workerFn,
  repeatEvery: REPEAT_EVERY,
})

console.log('env', ENV)

void (async () => {
  netatmo()
})()
