import got, { type Got } from 'got'

import { type NetatmoConfig } from './config.js'
import { type NetatmoToken, type StationsData } from './netatmo/interfaces.js'

export class Netatmo {
  private readonly deviceId: string
  private readonly clientId: string
  private readonly clientSecret: string
  private readonly gotInstance: Got

  private tokenExpiry: Date | null = null
  private accessToken: string
  private refreshToken: string

  constructor(opts: NetatmoConfig) {
    this.deviceId = opts.deviceId
    this.clientId = opts.clientId
    this.clientSecret = opts.clientSecret
    this.accessToken = opts.token
    this.refreshToken = opts.refreshToken

    this.gotInstance = got.extend({
      prefixUrl: 'https://api.netatmo.com',
      responseType: 'json',
    })
  }

  private async refreshTokenRequest() {
    return await this.gotInstance<NetatmoToken>('oauth2/token', {
      method: 'POST',
      form: {
        client_id: this.clientId,
        client_secret: this.clientSecret,
        grant_type: 'refresh_token',
        refresh_token: this.refreshToken,
      },
    })
  }

  private async stationsDataRequest() {
    await this.ensureTokenIsValid()

    return await this.gotInstance<StationsData>('api/getstationsdata', {
      headers: { Authorization: `Bearer ${this.accessToken}` },
      searchParams: { device_id: this.deviceId },
    })
  }

  private async ensureTokenIsValid() {
    const now = new Date()
    if (this.tokenExpiry !== null && this.tokenExpiry > now) return

    const tokenData = await this.refreshTokenRequest()
    this.accessToken = tokenData.body.access_token
    this.refreshToken = tokenData.body.refresh_token
    this.tokenExpiry = new Date(
      now.getTime() + tokenData.body.expires_in * 1000,
    )
  }

  async getStationsData() {
    await this.ensureTokenIsValid()
    return await this.stationsDataRequest()
  }
}
