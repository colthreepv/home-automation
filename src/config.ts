import { type ClientOptions } from '@influxdata/influxdb-client'
import { config } from 'dotenv'

const criticalException = (reason: string) => (context: any) => {
  if (context instanceof Error)
    console.error('Critical Exception occurred:', context.message)
  else console.error('Critical Exception occurred:', reason)
  console.error(context)
  process.exit(5)
}

const missingRequiredVariable = criticalException('Missing Required Variable')

export type APP_ENV = 'development' | 'production' | 'test'

export const ENV =
  process.env.NODE_ENV === 'production'
    ? 'production'
    : ((process.env.NODE_ENV ?? 'development') as APP_ENV)
// use of the .env file only in development
// eslint-disable-next-line @typescript-eslint/no-var-requires
if (ENV !== 'production') config({ path: `.env` })

const confBuilder = <K extends string>(vars: readonly K[]) => {
  return vars.reduce((result, v) => {
    // eslint-disable-next-line @typescript-eslint/no-throw-literal
    if (process.env[v] === undefined) throw missingRequiredVariable(v)
    result[v] = process.env[v] as string
    return result
    // eslint-disable-next-line
  }, {} as Record<K, string>)
}

// influx
interface InfluxConfig {
  org: string
  bucket: string
  client: ClientOptions
}
const { INFLUX_URL, INFLUX_TOKEN, INFLUX_ORG, INFLUX_BUCKET } = confBuilder([
  'INFLUX_URL',
  'INFLUX_TOKEN',
  'INFLUX_ORG',
  'INFLUX_BUCKET',
])
const influxConfig: InfluxConfig = {
  org: INFLUX_ORG,
  bucket: INFLUX_BUCKET,
  client: {
    url: INFLUX_URL,
    token: INFLUX_TOKEN,
  },
}

// Netatmo
export interface NetatmoConfig {
  deviceId: string
  clientId: string
  clientSecret: string
  token: string
  refreshToken: string
}
const {
  NETATMO_DEVICEID,
  NETATMO_CLIENT_ID,
  NETATMO_CLIENT_SECRET,
  NETATMO_TOKEN,
  NETATMO_REFRESH_TOKEN,
} = confBuilder([
  'NETATMO_DEVICEID',
  'NETATMO_CLIENT_ID',
  'NETATMO_CLIENT_SECRET',
  'NETATMO_TOKEN',
  'NETATMO_REFRESH_TOKEN',
])
const netatmoConfig: NetatmoConfig = {
  deviceId: NETATMO_DEVICEID,
  clientId: NETATMO_CLIENT_ID,
  clientSecret: NETATMO_CLIENT_SECRET,
  token: NETATMO_TOKEN,
  refreshToken: NETATMO_REFRESH_TOKEN,
}

export { influxConfig, netatmoConfig }
