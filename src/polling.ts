import { InfluxDB, type WriteApi } from '@influxdata/influxdb-client'

import { influxConfig } from './config.js'

const unrecoverableErrorCodes = ['ENOTFOUND', 'ECONNREFUSED']
const handleWriteFailed = (
  error: any,
  lines: string[],
  attempts: number,
): Promise<void> | void => {
  if (unrecoverableErrorCodes.includes(error.code) && attempts === 3) {
    console.error('suspecting unrecoverable error - killing')
    process.exit(1)
  }
}

interface InitQueueOptions {
  workerFn: (influxWrite: WriteApi) => () => Promise<void>
  repeatEvery?: number
}

const init = ({ workerFn, repeatEvery = 5000 }: InitQueueOptions) => {
  const influx = new InfluxDB(influxConfig.client)
  const influxWrite = influx.getWriteApi(
    influxConfig.org,
    influxConfig.bucket,
    undefined,
    { flushInterval: 1000, writeFailed: handleWriteFailed },
  )
  const fn = workerFn(influxWrite)
  void fn()
  const intervalValue = setInterval(() => void fn(), repeatEvery)

  const stop = () => {
    clearInterval(intervalValue)
  }
  return { stop }
}

export const createPollingJob = (opts: InitQueueOptions) => () => {
  const toCleanup: Array<() => void> = []
  const { stop } = init(opts)
  toCleanup.push(stop)

  console.log('process up')
  const gracefullyShutdown = () => {
    console.log('shutdown initiated')
    toCleanup.forEach((stop) => {
      stop()
    })
    setTimeout(() => process.exit(0), 500)
  }

  process.on('SIGINT', gracefullyShutdown)
  process.on('SIGTERM', gracefullyShutdown)
}
