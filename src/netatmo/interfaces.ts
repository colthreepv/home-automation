export interface StationsData {
  body: Body
  status: string
  time_exec: number
  time_server: number
}

interface Body {
  devices: Device[]
  user: User
}

interface Device {
  _id: string
  date_setup: number
  last_setup: number
  type: string
  last_status_store: number
  firmware: number
  last_upgrade: number
  wifi_status: number
  behavior: number
  user_owner: string[]
  reachable: boolean
  alarm_config: AlarmConfig
  co2_calibrating: boolean
  hardware_version: number
  customer_id: string
  data_type: string[]
  place: Place
  public_ext_data: boolean
  air_quality_available: boolean
  home_id: string
  home_name: string
  dashboard_data: DeviceDashboardData
  modules: Module[]
}

interface AlarmConfig {
  default_alarm: DefaultAlarm[]
}

interface DefaultAlarm {
  db_alarm_number: number
}

interface DeviceDashboardData {
  time_utc: number
  Temperature: number
  CO2: number
  Humidity: number
  Noise: number
  Pressure: number
  AbsolutePressure: number
  temp_trend: string
}

export interface Module {
  _id: string
  type: string
  last_setup: number
  module_name?: string
  data_type: string[]
  pluvio_scale_auget_to_mm: number
  battery_percent: number
  battery_level: string
  reachable: boolean
  firmware: number
  last_message: number
  last_seen: number
  rf_status: number
  battery_vp: number
  dashboard_data?: ModuleDashboardData
}

interface ModuleDashboardData {
  time_utc: number
  Temperature: number
  Humidity: number
  temp_trend: string
}

interface Place {
  altitude: number
  city: string
  street: string
  country: string
  timezone: string
  location: number[]
}

interface User {
  mail: string
  administrative: Administrative
  app_telemetry: boolean
}

interface Administrative {
  lang: string
  reg_locale: string
  country: string
  unit: number
  windunit: number
  pressureunit: number
  feel_like_algo: number
}

export interface NetatmoToken {
  access_token: string
  refresh_token: string
  scope: string[]
  expires_in: number
  expire_in: number
}
